Sympa and Exim4 on Debian: manual configuration required
========================================================

Debian users may have installed exim4 as the default MTA.

Unfortunately the default configuration of Exim4 will not allow Sympa to work
completely as it is configured at the moment (see bug #169102:
http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=169102)

And no automatic configuration customized for exim4 is done by Sympa either, so
you will have to do it manually after installation.

Two problems exist at the present time:
---------------------------------------

1) The 'sympa' and 'sympa-*' aliases defined in /etc/aliases are using pipes
but that's disabled in exim4 configuration in Debian (see "2.9. Using more
complex deliveries from alias files" in /usr/share/doc/exim4-base/README.Debian.gz)

It is then advised to check /var/log/exim4/mainlog for potential errors after
Sympa has been installed. The symptoms for pipe transport not working looks
like:
 == |/usr/lib/sympa/bin/queue sympa <sympa@yourserver> R=system_aliases defer (-30): pipe_transport unset in system_aliases router

One easy way to fix this problem is to enable pipes in /etc/aliases, even
though it may lead to security risks (note that the author of the present
guidelines is no exim4 specialist, so there may be better ways to do so).

We will provide guidelines below on how to enable those pipes in
/etc/aliases, but YOU MAY ONLY APPLY THEM AT YOUR OWN RISKS.

2) The mailing lists aliases are created by Sympa's alias_manager (see
'Automatic mailing-lists alias management' below) in /etc/mail/sympa/aliases,
but Exim has no clue, by default, that it has to use /etc/mail/sympa/aliases to
find list aliases there.

Why not configuring automatically during install of the package?
----------------------------------------------------------------

Because one may use other MTAs, and also because there are many options for
configuration of Exim4. See
http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=169102 for a discussion on
such options and also http://www.sympa.org/faq/exim)

A solution can be to add a 'router' in the configuration similar to the one for
/etc/aliases. That's the solution that is probably the most generic, as
supporting usual aliases management as done by most MTAs.  Other more complex
solutions exist, no longer relying on /etc/mail/sympa/aliases pipe aliases, but
which won't be detailed below.

In any case, depending on the way Exim4 is configured on your system, different
steps are necessary. Exim4 can have been configured either with "non-split"
configuration, which means that the file /etc/exim4/exim4.conf.template is used
as a template for the Exim4 configuration, or in "split configuration", which
means that the contents of the files in /etc/exim4/conf.d/ are used as a model
for its configuration (refer to /usr/share/doc/exim4/README.Debian for more
details). In any case, "dpkg-reconfigure exim4-config" will be needed to apply
configuration changes afterwards.

Configuring pipe aliases for Exim4 in "non-split" mode
------------------------------------------------------

1) Supporting pipe transport for system aliases (for aliases configured in
/etc/aliases)

The administrator will have to create (or modify) the file
/etc/exim4/exim4.conf.localmacros , and add the following macro definition
snippet in there:

#--------------
# Activating pipe transport in system_aliases router (pipes in /etc/aliases)
.ifndef SYSTEM_ALIASES_PIPE_TRANSPORT
SYSTEM_ALIASES_PIPE_TRANSPORT = address_pipe
.endif
.ifndef SYSTEM_ALIASES_USER
SYSTEM_ALIASES_USER = sympa
.endif
.ifndef SYSTEM_ALIASES_GROUP
SYSTEM_ALIASES_GROUP = sympa
.endif
#--------------

This will define 'address_pipe' as the transport for the 'system_aliases'
router (which handles /etc/aliases in which the 'sympa' alias is defined).

2) Adding a new router for lists aliases (for pipe aliases added by sympa in
/etc/mail/sympa/aliases)

The following snippet should be added right bellow the 'system_aliases'
definition in /etc/exim4/exim4.conf.template:

#--------------
# Using alias pipe definitions for the Sympa lists in /etc/mail/sympa/aliases
sympa_aliases:
  debug_print = "R: system_aliases for $local_part@$domain"
  driver = redirect
  domains = +local_domains
  allow_fail
  allow_defer
  data = ${lookup{$local_part}lsearch{/etc/mail/sympa/aliases}}
  user = sympa
  group = sympa
  pipe_transport = address_pipe
#--------------

Once /etc/exim4/exim4.conf.template is modified you will need to run the
'/usr/sbin/update-exim4.conf' command (man 8 update-exim4.conf), or
"dpkg-reconfigure exim4-config", to apply the template changes to the current
exim config.

Configuring pipe aliases for Exim4 in "split" mode
--------------------------------------------------

1) Supporting pipe transport for system aliases (for aliases configured in
/etc/aliases)

(This step is identical as for non-split mode, but repeated for lack of
ambiguity)

The administrator will have to create (or modify) the file
/etc/exim4/exim4.conf.localmacros , and add the following macro definition
snippet in there:

#--------------
# Activating pipe transport in system_aliases router (pipes in /etc/aliases)
.ifndef SYSTEM_ALIASES_PIPE_TRANSPORT
SYSTEM_ALIASES_PIPE_TRANSPORT = address_pipe
.endif
.ifndef SYSTEM_ALIASES_USER
SYSTEM_ALIASES_USER = sympa
.endif
.ifndef SYSTEM_ALIASES_GROUP
SYSTEM_ALIASES_GROUP = sympa
.endif
#--------------

This will define 'address_pipe' as the transport for the 'system_aliases'
router (which handles /etc/aliases in which the 'sympa' alias is defined).

2) Adding a new router for lists aliases (for pipe aliases added by sympa in
/etc/mail/sympa/aliases)

A file such as '/etc/exim4/conf.d/router/450_local-config_sympa_aliases' should
be created, containing:

#--------------
# Using alias pipe definitions for the Sympa lists in /etc/mail/sympa/aliases
sympa_aliases:
  debug_print = "R: system_aliases for $local_part@$domain"
  driver = redirect
  domains = +local_domains
  allow_fail
  allow_defer
  data = ${lookup{$local_part}lsearch{/etc/mail/sympa/aliases}}
  user = sympa
  group = sympa
  pipe_transport = address_pipe
#--------------

Then running the '/usr/sbin/update-exim4.conf' command (man 8
update-exim4.conf), or "dpkg-reconfigure exim4-config" will apply the
changes to the current exim config.

Again, there are other options for configuring sympa mailing-lists support for
Exim4, but which haven't been documented here, as requiring more Exim
expertise. See http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=169102 for more
details.


 -- Olivier Berger <obergix@debian.org>  Sun, 21 Aug 2011 09:06:23 +0000
